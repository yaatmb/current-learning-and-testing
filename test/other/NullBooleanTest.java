package other;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Тест, который проверяет, выводит ли метод main ожидаемый нами NullPointerException
 */
public class NullBooleanTest {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test(expected = NullPointerException.class)
    public void testMain() throws Exception {
        NullBoolean.main(null);
    }
}