package chapter20;

import java.io.File;

/**
 * Использование каталогов
 */
public class DirList {
    public static void main(String[] args) {
        String dirname = "src/chapter19";
        File mainFile = new File(dirname);
        if (mainFile.isDirectory()) {
            System.out.println("Каталог " + dirname);
            String filenames[] = mainFile.list();
            for (String filename : filenames) {
                File file = new File(dirname + "/" + filename);
                if (file.isDirectory()) {
                    System.out.println("\t" + filename + " является каталогом");
                } else {
                    System.out.println("\t" + filename + " является файлом");
                }
            }
        }
    }
}
