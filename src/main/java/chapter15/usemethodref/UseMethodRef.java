package chapter15.usemethodref;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Демонстрация использования ссылки, чтобы найти
 * максимальное значение в коллекции
 */
public class UseMethodRef {
    static int compareMC(MyClass a, MyClass b) {
        return a.getVal() - b.getVal();
    }

    public static void main(String[] args) {
        ArrayList<MyClass> al = new ArrayList<MyClass>();
        al.add(new MyClass(1));
        al.add(new MyClass(4));
        al.add(new MyClass(2));
        al.add(new MyClass(9));
        al.add(new MyClass(3));
        al.add(new MyClass(7));

        MyClass maxValObj;

        // Первый вариант - используем анонимный класс компаратора
        maxValObj = Collections.max(al, new Comparator<MyClass>() {
            @Override
            public int compare(MyClass o1, MyClass o2) {
                return o1.getVal() - o2.getVal();
            }
        });
        System.out.println("Первый вариант (анонимный класс): " + maxValObj.getVal());

        // Второй вариант - используем лямбда-выражение вместо компаратора
        maxValObj = Collections.max(al, (o1, o2) -> o1.getVal() - o2.getVal());
        System.out.println("Второй вариант (лямбда-выражение): " + maxValObj.getVal());

        // Третий вариант - используем ссылку на метод, из которой автоматически
        // создастся лямбда выражение
        maxValObj = Collections.max(al, UseMethodRef::compareMC);
        System.out.println("Третий вариант (ссылка на метод): " + maxValObj.getVal());
    }
}
