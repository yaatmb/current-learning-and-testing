package chapter15.usemethodref;

/**
 * Класс с одним полем
 */
public class MyClass {
    private int val;

    public MyClass(int val) {
        this.val = val;
    }

    public int getVal() {
        return val;
    }
}
