package chapter15.lambdademo;

/**
 * Функциональный интерфейс
 */
@FunctionalInterface
public interface MyNumber {
    double getValue();
}
