package chapter15.varcapture;

/**
 * Демонстрация захвата переменной
 */
public class VarCapture {
    public static void main(String[] args) {
        String month = "крякобрябрь";
        Season season = () -> {
            switch (month) {
                case "декабрь":
                case "январь":
                case "февраль":
                    System.out.println(month + " - это зима");
                    break;
                case "март":
                case "апрель":
                case "май":
                    System.out.println(month + " - это весна");
                    break;
                case "июнь":
                case "июль":
                case "август":
                    System.out.println(month + " - это лето");
                    break;
                case "сентябрь":
                case "октябрь":
                case "ноябрь":
                    System.out.println(month + " - это осень");
                    break;
                default:
                    System.out.println(month + " - мне такой месяц неизвестен");
            }
        };
        season.show();
    }
}
