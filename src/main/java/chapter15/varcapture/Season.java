package chapter15.varcapture;

/**
 * Функциональный интерфейс для вывода сезона
 */
@FunctionalInterface
public interface Season {
    void show();
}
