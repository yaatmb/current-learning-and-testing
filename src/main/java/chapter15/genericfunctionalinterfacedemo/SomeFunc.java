package chapter15.genericfunctionalinterfacedemo;

/**
 * Обобщённый функциональный интерфейс
 */
public interface SomeFunc<T> {
    T func(T t);
}
