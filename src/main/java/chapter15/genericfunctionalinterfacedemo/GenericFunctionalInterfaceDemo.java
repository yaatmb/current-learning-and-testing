package chapter15.genericfunctionalinterfacedemo;

/**
 * Демонстрация применения обобщённого функционального интерфейса
 */
public class GenericFunctionalInterfaceDemo {
    public static void main(String[] args) {
        SomeFunc<String> reverse = (str) -> {
            String result = "";
            for (int i = str.length() - 1; i >= 0; i--) {
                result += str.charAt(i);
            }
            return result;
        };
        System.out.println("Лямбда обращается в " + reverse.func("Лямбда"));
        System.out.println("Выражение обращается в " + reverse.func("Выражение"));

        SomeFunc<Integer> factorial = (n) -> {
            int result = 1;
            for (int i = 2; i <= n; i++) {
                result *= i;
            }
            return result;
        };

        System.out.println("Факториал числа 3 равен " + factorial.func(3));
        System.out.println("Факториал числа 5 равен " + factorial.func(5));
    }
}
