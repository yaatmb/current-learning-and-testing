package chapter15.instancemethwithobjectrefdemo;

/**
 * Функциональный интерфейс, принимающий два ссылочных
 * аргумента и возвращающий логическое значение
 */
public interface MyFunc<T> {
    boolean func(T v1, T v2);
}
