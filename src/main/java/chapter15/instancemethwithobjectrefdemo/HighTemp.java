package chapter15.instancemethwithobjectrefdemo;

/**
 * Класс для хранения максимальной температуры за день
 */
public class HighTemp {
    private int hTemp;

    HighTemp(int hTemp) {
        this.hTemp = hTemp;
    }

    boolean sameTemp(HighTemp highTemp) {
        return hTemp == highTemp.hTemp;
    }

    boolean lessThanTemp(HighTemp highTemp) {
        return hTemp < highTemp.hTemp;
    }
}
