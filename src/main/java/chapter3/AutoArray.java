package chapter3;

/**
 * Усовершенствованная версия программы Array
 */
public class AutoArray {
    public static void main(String[] args) {
        int month_days[] = {31, 23, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        System.out.println("В апреле " + month_days[3] + " дней.");
    }
}
