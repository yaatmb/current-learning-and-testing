package chapter19.mytimertask;

import java.util.TimerTask;

/**
 * Задание
 */
public class MyTimerTask extends TimerTask {
    @Override
    public void run() {
        System.out.println("Задание по таймеру выполняется.");
    }
}
