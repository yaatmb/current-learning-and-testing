package chapter19.mytimertask;

import java.util.Timer;

/**
 * Демонстрация применения классов TimerTask и Timer
 */
public class TTest {
    public static void main(String[] args) {
        MyTimerTask myTask = new MyTimerTask();
        Timer myTimer = new Timer();
        myTimer.schedule(myTask, 1000, 500);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        myTimer.cancel();
    }
}
