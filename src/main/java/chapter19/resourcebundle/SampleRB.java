package chapter19.resourcebundle;

import java.util.ListResourceBundle;

/**
 * Комплект ресурсов на русском языке
 */
public class SampleRB extends ListResourceBundle {
    @Override
    protected Object[][] getContents() {
        Object[][] resources = new Object[3][2];

        resources[0][0] = "Title";
        resources[0][1] = "Моя программа";

        resources[1][0] = "StartText";
        resources[1][1] = "Старт";

        resources[2][0] = "StopText";
        resources[2][1] = "Стоп";

        return resources;
    }
}
