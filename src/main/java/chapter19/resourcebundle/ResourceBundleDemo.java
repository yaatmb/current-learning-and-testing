package chapter19.resourcebundle;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Демонстрация применения комплекта ресурсов
 */
public class ResourceBundleDemo {
    public static void main(String[] args) {
        String resourceBundleBaseName = SampleRB.class.getCanonicalName();
        ResourceBundle rb = ResourceBundle.getBundle(resourceBundleBaseName);
        System.out.println("Версия программы с языком по-умолчанию:");
        System.out.println("Строка по ключу Title: " + rb.getString("Title"));
        System.out.println("Строка по ключу StartText: " + rb.getString("StartText"));
        System.out.println("Строка по ключу StopText: " + rb.getString("StopText"));

        System.out.println();

        rb = ResourceBundle.getBundle(resourceBundleBaseName, Locale.ENGLISH);
        System.out.println("Английская версия программы:");
        System.out.println("Строка по ключу Title: " + rb.getString("Title"));
        System.out.println("Строка по ключу StartText: " + rb.getString("StartText"));
        System.out.println("Строка по ключу StopText: " + rb.getString("StopText"));
    }
}
