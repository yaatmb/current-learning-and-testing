package chapter19.resourcebundle;

import java.util.ListResourceBundle;

/**
 * Комплект ресурсов на английском языке
 */
public class SampleRB_en extends ListResourceBundle {
    @Override
    protected Object[][] getContents() {
        Object[][] resources = new Object[3][2];

        resources[0][0] = "Title";
        resources[0][1] = "My Program";

        resources[1][0] = "StartText";
        resources[1][1] = "Start";

        resources[2][0] = "StopText";
        resources[2][1] = "Stop";

        return resources;
    }
}
