package chapter19.observers;

import java.util.Observable;
import java.util.Observer;

/**
 * Наблюдатель со своим позывным
 */
public class SecretAgent implements Observer {

    private String name;

    public SecretAgent(String name) {
        this.name = name;
    }

    @Override
    public void update(Observable o, Object arg) {
        System.out.println(name + " на связи. Объект изменил свои координаты.");
    }
}
