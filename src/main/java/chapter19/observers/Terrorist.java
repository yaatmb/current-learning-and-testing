package chapter19.observers;

import java.util.Observable;

/**
 * Террорист
 */
public class Terrorist extends Observable {
    /** Переместить террориста */
    public void changeCoord() {
        setChanged();
        notifyObservers();
    }
}
