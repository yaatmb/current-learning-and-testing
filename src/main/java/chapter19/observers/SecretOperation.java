package chapter19.observers;

/**
 * Секретная операция
 */
public class SecretOperation {
    public static void main(String[] args) {
        Terrorist terrorist = new Terrorist();
        terrorist.addObserver(new SecretAgent("Стриж"));
        terrorist.addObserver(new SecretAgent("Воробей"));
        terrorist.addObserver((o, arg) -> System.out.println("Я случайный лямбда-наблюдатель. Что тут происходит!? А-а-а-а!!!"));
        terrorist.addObserver(new SecretAgent("Чайка"));
        terrorist.addObserver(new SecretAgent("Ястреб"));
        System.out.println("За террористом наблюдают " + terrorist.countObservers() + " агентов.");
        terrorist.changeCoord();
    }
}
