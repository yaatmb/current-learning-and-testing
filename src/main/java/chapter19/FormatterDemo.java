package chapter19;

import java.util.Calendar;
import java.util.Formatter;

/**
 * Демонстраци работы форматтера
 */
public class FormatterDemo {
    public static void main(String[] args) {
        System.out.println(new Formatter().format("%X", 250));
        System.out.println(new Formatter().format("%06.2f", 35.458954));
        Calendar calendar = Calendar.getInstance();
        System.out.println(new Formatter().format("Сегодня %te %<tB %<tY года", calendar));
    }
}
