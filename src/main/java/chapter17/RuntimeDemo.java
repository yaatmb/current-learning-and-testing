package chapter17;

/**
 * Некоторые методы класса Runtime
 */
public class RuntimeDemo {
    public static void main(String[] args) {
        Runtime runtime = Runtime.getRuntime();
        long mem1, mem2;
        Integer someints[] = new Integer[1000];

        System.out.println("Всего памяти:                       " + runtime.totalMemory());
        mem1 = runtime.freeMemory();
        System.out.println("Свободной пмяти исходно:            " + mem1);
        runtime.gc();
        mem1 = runtime.freeMemory();
        System.out.println("Свободной памяти после очистки:     " + mem1);

        for (int i = 0; i < 1000; i++) {
            someints[i] = i;
        }

        mem2 = runtime.freeMemory();
        System.out.println("Свободной памяти после выделения:   " + mem2);
        System.out.println("Использовано памяти для выделения:  " + (mem1 - mem2));

        for (int i = 0; i < 1000; i++) {
            someints[i] = null;
        }
        runtime.gc();

        mem2 = runtime.freeMemory();
        System.out.println("Свободной памяти после очистки \n отвергнутых объектов типа Integer: " + mem2);
    }
}
