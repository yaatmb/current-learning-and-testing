package chapter17;

import java.io.IOException;

/**
 * Демонстрация запуска notepad через объект класса ProcessBuilder
 */
public class ProcessBuilderDemo {
    public static void main(String[] args) {
        ProcessBuilder processBuilder = new ProcessBuilder("notepad", "README.md");
        try {
            processBuilder.start();
        } catch (IOException e) {
            System.out.println("Ошибка запуска notepad");
        }
    }
}
