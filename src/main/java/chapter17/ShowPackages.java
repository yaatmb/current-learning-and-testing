package chapter17;

/**
 * Выводит информацию о пакетах
 */
public class ShowPackages {
    public static void main(String[] args) {
        for (Package aPackage : Package.getPackages()) {
            System.out.println(aPackage.getName() + " " +
                    aPackage.getImplementationTitle() + " " +
                    aPackage.getImplementationVendor() + " " +
                    aPackage.getImplementationVersion()
            );
        }
    }
}
