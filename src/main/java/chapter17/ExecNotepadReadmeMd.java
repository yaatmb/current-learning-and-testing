package chapter17;

import java.io.IOException;

/**
 * Демонстрация запуска программ методом exec()
 */
public class ExecNotepadReadmeMd {
    public static void main(String[] args) {
        Runtime runtime = Runtime.getRuntime();
        int c = '0';
        while (c < '1' || c > '2') {
            System.out.println("Введите 1 или 2 и нажмите Enter");
            try {
                c = System.in.read();
            } catch (IOException e) {
                System.out.println("Ошибка чтения из ");
            }
        }
        switch (c) {
            case '1':
                try {
                    Process notepad = runtime.exec("Notepad README.md");
                    notepad.waitFor();
                } catch (IOException e) {
                    System.out.println("Ошибка запуска Notepad");
                } catch (InterruptedException e) {
                    System.out.println("Ошибка: Процесс Notepad уже завершился");
                }
                break;
            case '2':
                try {
                    Process gitVersion = runtime.exec("git version");
                    gitVersion.waitFor();
                    System.out.println("git version возвратил " + gitVersion.exitValue());
                } catch (IOException e) {
                    System.out.println("Ошибка запуска git version");
                } catch (InterruptedException e) {
                    System.out.println("Ошибка: Процесс git version уже завершился");
                }
                break;
        }
    }
}
