package chapter17;

import java.util.Arrays;
import java.util.List;

/**
 * Демонстрация метода forEach и лямбда выражений
 */
public class ForEachDemo {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("Демонстрация метода forEach и лямбда выражений".split(" "));
        list.sort(String::compareToIgnoreCase);
        list.forEach(System.out::println);
    }
}
