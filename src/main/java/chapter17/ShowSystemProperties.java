package chapter17;

/**
 * Отображение всех основных свойств окружения
 */
public class ShowSystemProperties {
    public static void main(String[] args) {
        System.getProperties().list(System.out);
    }
}
