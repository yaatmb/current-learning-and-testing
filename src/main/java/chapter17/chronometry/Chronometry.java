package chapter17.chronometry;

/**
 * Измерение времени выполнения программы с использованием
 */
public class Chronometry {
    public static void main(String[] args) {
        // На первый запуск функции уходит больше времени
        for (int i = 0; i < 3; i++) {
            System.out.println("Измерение в миллисекундах");
            System.out.printf("В пересчёте на секунды: %.2f секунд\n", chronometry(System::currentTimeMillis) / 1000.0);
            System.out.println("Измерение в наносекундах");
            System.out.printf("В пересчёте на секунды: %.2f секунд\n", chronometry(System::nanoTime) / 1000000000.0);
            System.out.println("Измерение в миллисекундах");
        }
    }

    private static long chronometry(Elapsed elapsed) {
        System.out.println("Измерение времени перебора от 0 до 100000000");
        long start = elapsed.getValue();
        for (long i = 0; i < 100000000L; i++) {
            System.nanoTime();
        }
        long end = elapsed.getValue();
        long result = end - start;
        System.out.println("Время выполнения: " + result);
        return result;
    }
}
