package chapter17.chronometry;

/**
 * Функциональный интерфейс
 */
@FunctionalInterface
public interface Elapsed {
    long getValue();
}
