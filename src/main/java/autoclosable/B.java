package autoclosable;

import java.io.Closeable;

/**
 * ����� �������, ���������� ������ ����� �������
 */
public class B implements Closeable {

    private A a;

    B(A a) {
        System.out.println("new B(a)");
        this.a = a;
    }

    A getA() {
        System.out.println("getA");
        return a;
    }

    @Override
    public void close() {
        System.out.println("b.close()");
    }
}
