package autoclosable;

import java.io.Closeable;

/**
 * ����� �������, ���������� ������ � �������
 */
public class C implements Closeable {

    private B b;

    C(B b) {
        System.out.println("new C(b)");
        this.b = b;
    }

    B getB() {
        System.out.println("getB");
        return b;
    }

    @Override
    public void close() {
        System.out.println("c.close()");
    }
}
