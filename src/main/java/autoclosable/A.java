package autoclosable;

import java.io.Closeable;

/**
 * ����� ������� (����� ��� �����������)
 */
public class A implements Closeable {

    public A() {
        System.out.println("new A()");
    }

    @Override
    public void close() {
        System.out.println("a.close()");
    }
}
