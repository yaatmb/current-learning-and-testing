package autoclosable;

/**
 * ��������, ����� ������� ����� ������� � ����� ����� ������������� �������� ��������
 */
public class ShowCloseable {

    public static void main(String[] args) {
        System.out.println("create objects");
        C c = new C(new B(new A()));
        System.out.println("try-with-resources");
        try (A a = c.getB().getA()) {
            System.out.println("in try-with-resources");
        } finally {
            System.out.println("finally");
        }
        System.out.println("end");
    }
}
