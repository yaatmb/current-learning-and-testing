import java.util.HashMap;
import java.util.Map;

/**
 * Created by unlim on 30.06.17.
 */
public abstract class Digest {

    private static final Map<byte[], byte[]> cache = new HashMap<>(); // ��� static final, ���� �� ������� - ��������� �������

    public byte[] digest(byte[] input) {
        byte[] result = cache.get(input);
        if (result == null) {
            synchronized (cache) {
                System.out.println("������ ����� ������������� Digest.digest");
                byte[] clone = input.clone();
                result = cache.computeIfAbsent(clone, this::doDigest);
                for (int i = 0; i < clone.length; i++) {
                    System.out.println("clone[i] = " + clone[i]);
                }
            }
        }
        System.out.println("cache.size(): " + cache.size());
        return result;
    }

    protected abstract byte[] doDigest(byte[] input);

    public static void main(String[] args) {
        for (int i = 0; i < 1000; i++) {
            new MyThread().run();
        }
        for (byte[] bytes : cache.keySet()) {
            for (byte aByte : bytes) {
                System.out.print(aByte);
            }
        }
    }
}

class MyDigest extends Digest {

    private static MyDigest instance = null;

    public static MyDigest getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new MyDigest();
        }
    }

    @Override
    protected byte[] doDigest(byte[] input) {
        for (int i = 0; i < input.length; i++) {
            input[i]++;
        }
        return input;
    }
}

class MyThread implements Runnable {

    @Override
    public void run() {
        byte[] result = MyDigest.getInstance().digest(new byte[]{1, 2, 3, 4, 5});
    }
}