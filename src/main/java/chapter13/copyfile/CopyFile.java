package chapter13.copyfile;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Программа позволяет скопировать содержимое файла.
 * Для копирования какого-либо файла нужно выполнить в консоли команду java CopyFile First.txt Second.txt
 * First.txt - файл, из которого копируются данные, а Second.txt - файл, в который копируются данные.
 */
public class CopyFile {
    public static void main(String[] args) {
        int i;

        // сначала убедимся, что указаны имена обоих файлов
        if (args.length != 2) {
            System.out.println("Использование: CopyFile исходный_файл конечный_файл");
            return;
        }

        // Копировать файл
        try (FileInputStream fin = new FileInputStream(args[0]);
             FileOutputStream fout = new FileOutputStream(args[1])) {
            do {
                i = fin.read();
                if (i != -1) {
                    fout.write(i);
                }
            } while (i != -1);
        } catch (IOException e) {
            System.out.println("Ошибка ввода-вывода: " + e);
        }
    }
}
