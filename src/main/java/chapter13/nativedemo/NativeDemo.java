package chapter13.nativedemo;

/**
 * Пример применения платформенно-ориентированного метода
 */
public class NativeDemo {
    int i;

    public static void main(String[] args) {
        NativeDemo ob = new NativeDemo();
        ob.i = 10;
        System.out.println("Содержимое переменной ob.i перед вызовом платформенно-ориентированного метода: " + ob.i);
        ob.test();
        System.out.println("Содержимое переменной ob.i после вызова платформенно-ориентированного метода: " + ob.i);
    }

    // Платформенно-ориентированный метод
    public native void test();

    // Загрузить библиотеку DLL, содержащую статический метод
    static {
        System.loadLibrary("chapter13_nativedemo_NativeDemo");
    }
}
