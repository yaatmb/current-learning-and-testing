package chapter13.simpleapplet;

import java.applet.Applet;
import java.awt.*;

/**
 * Простейший апплет
 */
public class SimpleApplet extends Applet {
    @Override
    public void paint(Graphics g) {
        g.drawString("Простейший апплет", 20, 20);
    }
}
