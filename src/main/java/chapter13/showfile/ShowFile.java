package chapter13.showfile;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Программа позволяет просмотреть содержимое текстового файла.
 * Для просмотра какого-либо текстового файла нужно выполнить в консоли команду java ShowFile HelloWorld.txt
 */
public class ShowFile {
    public static void main(String[] args) {
        int i;

        // сначала убедимс, что имя файла указано
        if (args.length != 1) {
            System.out.println("Использование: ShowFile имя_файла");
            return;
        }

        /* Сначала файл открывается.
        Далее из него считаются символы до тех пор,
        пока не встретится признак конца файла. */
        try (FileInputStream fin = new FileInputStream(args[0])) {
            do {
                i = fin.read();
                if (i != -1) System.out.print((char) i);
            } while (i != -1);
        } catch (FileNotFoundException e) {
            System.out.println("Невозможно открыть файл");
        } catch (IOException e) {
            System.out.println("Ошибка чтения из файла");
        }
    }
}
