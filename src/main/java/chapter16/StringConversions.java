package chapter16;

/**
 * Преобразование числа в двоичную, восьмеричную и шестнадцатеричную формы
 */
public class StringConversions {
    public static void main(String[] args) {
        int num = 19648;
        System.out.println("Integer.toBinaryString(num) = " + Integer.toBinaryString(num));
        System.out.println("Integer.toOctalString(num) = " + Integer.toOctalString(num));
        System.out.println("Integer.toHexString(num) = " + Integer.toHexString(num));
    }
}
