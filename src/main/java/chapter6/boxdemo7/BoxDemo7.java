package chapter6.boxdemo7;

/**
 * В этой программе применяется метод, введённый в класс Box
 */
public class BoxDemo7 {
    public static void main(String[] args) {
        // Объявить, выделить память и инициализировать объекты типа Box
        Box mybox1 = new Box(10, 20, 15);
        Box mybox2 = new Box(3, 6, 9);

        double vol;

        // получить объём первого параллепипеда
        vol = mybox1.volume();
        System.out.println("Объём равен " + vol);

        // получить объём второго параллепипеда
        vol = mybox2.volume();
        System.out.println("Объём равен " + vol);
    }
}

/**
 * Параллепипед
 */
class Box {
    double width;
    double height;
    double depth;

    // Это конструктор класса Box
    Box(double w, double h, double d) {
        width = w;
        height = h;
        depth = d;
    }

    // рассчитать и возвратить объём
    double volume() {
        return width * height * depth;
    }
}