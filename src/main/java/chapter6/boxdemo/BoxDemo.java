package chapter6.boxdemo;

/**
 * Программа, использующая класс Box
 */
public class BoxDemo {
    public static void main(String[] args) {
        Box mybox = new Box();
        double vol;

        // присвоить значение переменным экземпляра mybox
        mybox.width = 10;
        mybox.height = 20;
        mybox.depth = 15;

        // расчитать объём параллепипеда
        vol = mybox.width * mybox.height * mybox.depth;
        System.out.println("Объём равен " + vol);
    }
}

/**
 * Параллепипед
 */
class Box {
    double width;
    double height;
    double depth;
}