package chapter6.boxdemo3;

/**
 * В этой программе применяется метод, введённый в класс Box
 */
public class BoxDemo3 {
    public static void main(String[] args) {
        Box mybox1 = new Box();
        Box mybox2 = new Box();

        // присвоить значение переменным экземпляра mybox1
        mybox1.width = 10;
        mybox1.height = 20;
        mybox1.depth = 15;

        // присвоить другие значение переменным экземпляра mybox2
        mybox2.width = 3;
        mybox2.height = 6;
        mybox2.depth = 9;

        // вывести объём первого параллепипеда
        mybox1.volume();

        // вывести объём второго параллепипеда
        mybox2.volume();
    }
}

/**
 * Параллепипед
 */
class Box {
    double width;
    double height;
    double depth;

    void volume() {
        System.out.print("Объём равен ");
        System.out.println(width * height * depth);
    }
}