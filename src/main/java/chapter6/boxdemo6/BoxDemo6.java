package chapter6.boxdemo6;

/**
 * В этой программе применяется метод, введённый в класс Box
 */
public class BoxDemo6 {
    public static void main(String[] args) {
        // Объявить, выделить память и инициализировать объекты типа Box
        Box mybox1 = new Box();
        Box mybox2 = new Box();

        double vol;

        // получить объём первого параллепипеда
        vol = mybox1.volume();
        System.out.println("Объём равен " + vol);

        // получить объём второго параллепипеда
        vol = mybox2.volume();
        System.out.println("Объём равен " + vol);
    }
}

/**
 * Параллепипед
 */
class Box {
    double width;
    double height;
    double depth;

    // Это конструктор класса Box
    Box() {
        System.out.println("Конструирование объекта Box");
        width = 10;
        height = 10;
        depth = 10;
    }

    // рассчитать и возвратить объём
    double volume() {
        return width * height * depth;
    }
}