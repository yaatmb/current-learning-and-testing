package chapter6.boxdemo2;

/**
 * В этой программе объявляются два объекта класса Box
 */
public class BoxDemo2 {
    public static void main(String[] args) {
        Box mybox1 = new Box();
        Box mybox2 = new Box();
        double vol;

        // присвоить значение переменным экземпляра mybox1
        mybox1.width = 10;
        mybox1.height = 20;
        mybox1.depth = 15;

        // присвоить другие значение переменным экземпляра mybox2
        mybox2.width = 3;
        mybox2.height = 6;
        mybox2.depth = 9;

        // расчитать объём первого параллепипеда
        vol = mybox1.width * mybox1.height * mybox1.depth;
        System.out.println("Объём равен " + vol);

        // расчитать объём второго параллепипеда
        vol = mybox2.width * mybox2.height * mybox2.depth;
        System.out.println("Объём равен " + vol);

    }
}

/**
 * Параллепипед
 */
class Box {
    double width;
    double height;
    double depth;
}
