package chapter12.myanno;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * Класс с аннотированным методом
 */
public class MyAnnoMethod {
    @MyAnno(str = "Пример аннотации", val = 100)
    public static void myMeth() {

    }

    public static void main(String[] args) {
        System.out.println("Вывести все методы класса MyAnnoMethod.");
        /*MyAnnoMethod myAnnoMethod = new MyAnnoMethod();
        for (Method method : myAnnoMethod.getClass().getDeclaredMethods()) {*/
        // Можно использовать и такой вариант получения методов класса
        for (Method method : MyAnnoMethod.class.getDeclaredMethods()) {
            System.out.println(method);
            for (Annotation annotation : method.getAnnotations()) {
                System.out.println("-> " + annotation);
                for (Annotation annotation1 : annotation.annotationType().getAnnotations()) {
                    System.out.println("--> " + annotation1);
                    for (Annotation annotation2 : annotation1.annotationType().getAnnotations()) {
                        System.out.println("---> " + annotation2);
                    }
                }
            }
        }
    }
}
