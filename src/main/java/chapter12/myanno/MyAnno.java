package chapter12.myanno;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Пример аннотациии
 */
@Retention(RetentionPolicy.RUNTIME)
@interface MyAnno {
    String str();

    int val();
}
