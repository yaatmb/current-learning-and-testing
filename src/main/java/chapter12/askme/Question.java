package chapter12.askme;

import java.util.Random;

import static chapter12.askme.Answers.*;

/**
 * Класс, задающий вопрос
 */
public class Question {
    Random random = new Random();

    Answers ask() {
        int prob = (int) (100 * random.nextDouble());

        if (prob < 15) {
            return MAYBE;
        } else if (prob < 30) {
            return NO;
        } else if (prob < 60) {
            return YES;
        } else if (prob < 75) {
            return LATER;
        } else if (prob < 98) {
            return SOON;
        } else {
            return NEVER;
        }
    }
}
