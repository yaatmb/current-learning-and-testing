package chapter12.askme;

/**
 * Программа принятия решений
 */
public class AskMe {
    private static void answer(Answers result) {
        System.out.println(result);
    }

    public static void main(String[] args) {
        Question q = new Question();
        for (int i = 0; i < 10; i++) {
            answer(q.ask());
        }
    }
}
