package chapter12.askme;

/**
 * Варианты ответов
 */
public enum Answers {
    NO, YES, MAYBE, LATER, SOON, NEVER
}
