package streams;

import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by unlim on 28.06.17.
 */
public class Example1 {
    public static void main(String[] args) {
        IntStream stream = IntStream.range(0, 10);
        Stream stream2 = (Stream) stream;
        System.out.println(stream2.limit(5).count());
    }
}
