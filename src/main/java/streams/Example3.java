package streams;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by unlim on 28.06.17.
 */
public class Example3 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("1");
        list.add("2");
        list.stream().sorted().
                peek(list::remove).
                forEach(System.out::println);
        System.out.println(list.size());
    }
}
