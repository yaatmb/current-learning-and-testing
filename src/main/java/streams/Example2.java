package streams;

import java.util.stream.IntStream;

/**
 * Created by unlim on 28.06.17.
 */
public class Example2 {
    public static void main(String[] args) {
        IntStream.iterate(0, i -> (int) Math.round(Math.random() * 10)).limit(150).distinct().sorted().forEach(System.out::println);
    }
}
