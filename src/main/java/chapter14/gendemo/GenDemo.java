package chapter14.gendemo;

/**
 * Демонстрация применения обобщённого класса
 */
public class GenDemo {
    public static void main(String[] args) {
        Gen<Integer> iObject;
        iObject = new Gen<>(88);
        iObject.showType();
        int v = iObject.getObject();
        System.out.println("v = " + v);
        System.out.println();

        Gen<String> strObject = new Gen<>("Тест обобщений");
        strObject.showType();
        String str = strObject.getObject();
        System.out.println("str = " + str);
    }
}
