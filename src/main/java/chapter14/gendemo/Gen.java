package chapter14.gendemo;

/**
 * Простой обобщённый класс.
 * Здесь T обозначает параметр типа,
 * который будет заменён реальным типом
 * при создании объекта типа Gen
 */
public class Gen<T> {
    /** Объект типа T */
    T object;

    /** Конструктор принимает ссылку на объект типа T */
    Gen(T object) {
        this.object = object;
    }

    /** Возвращает объект типа T */
    T getObject() {
        return object;
    }

    /** Показывает тип T */
    void showType() {
        System.out.println("Типом T является " + object.getClass().getName());
    }
}
