package chapter14.twogendemo;

/**
 * Простой обобщённый класс с двумя параметрами типа: T и V
 */
public class TwoGen<T, V> {
    T object1;
    V object2;

    /** Конструктор, принимающий ссылки на объекты типа T и V */
    public TwoGen(T object1, V object2) {
        this.object1 = object1;
        this.object2 = object2;
    }

    /** Показывает типы T и V */
    public void showTypes() {
        System.out.println("Тип T: " + object1.getClass().getName());
        System.out.println("Тип V: " + object2.getClass().getName());
    }

    public T getObject1() {
        return object1;
    }

    public V getObject2() {
        return object2;
    }
}
