package chapter14.twogendemo;

/**
 * Демонстрация применения класса TwoGen
 */
public class TwoGenDemo {
    public static void main(String[] args) {
        TwoGen<Integer, String> twoGen = new TwoGen<>(88, "Обобщения");
        twoGen.showTypes();
        System.out.println("twoGen.getObject1() = " + twoGen.getObject1());
        System.out.println("twoGen.getObject2() = " + twoGen.getObject2());
    }
}
