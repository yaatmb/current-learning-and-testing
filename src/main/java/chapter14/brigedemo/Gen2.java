package chapter14.brigedemo;

/**
 * Подкласс обобщённого класса
 *
 * Если в командной строке выполнить команду javap chapter14.brigedemo.Gen2,
 * результат будет следующий:
 *
 * Compiled from "Gen2.java"
 * public class chapter14.brigedemo.Gen2 extends chapter14.brigedemo.Gen<java.lang.String> {
 *   public chapter14.brigedemo.Gen2(java.lang.String);
 *   public java.lang.String getObject();
 *   public java.lang.Object getObject();
 * }
 *
 * Можно увидеть, что java создаст два метода с одинаковой сигнатурой,
 * но разными возвращаемыми значениями.
 * В исходном коде программы так писать нельзя, это привело бы к ошибке,
 * но т.к. это происходит не в исходном коде, то никаких сложностей не возникает,
 * и виртуальная машина Java находит правильный выход из данной ситуации.
 */
public class Gen2 extends Gen<String> {

    public Gen2(String object) {
        super(object);
    }

    @Override
    public String getObject() {
        System.out.print("Вызван метод String getObject(): ");
        return super.getObject();
    }
}
