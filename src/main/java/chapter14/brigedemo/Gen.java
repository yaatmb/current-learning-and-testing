package chapter14.brigedemo;

/**
 * Обобщённый класс
 */
public class Gen<T> {
    T object;

    public Gen(T object) {
        this.object = object;
    }

    public T getObject() {
        return object;
    }
}
