package chapter14.brigedemo;

/**
 * Демонстрация ситуации, когда требуется мостовой метод
 */
public class BridgeDemo {
    public static void main(String[] args) {
        Gen2 strOb2 = new Gen2("Тест обобщений");
        System.out.println(strOb2.getObject());
    }
}
