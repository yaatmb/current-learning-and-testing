package chapter14.boundedwildcard;

/**
 * Трёхмерные координаты
 */
public class ThreeD extends TwoD {
    int z;

    public ThreeD(int x, int y, int z) {
        super(x, y);
        this.z = z;
    }
}
