package chapter14.boundedwildcard;

/**
 * Четырёхмерные координаты
 */
public class FourD extends ThreeD {
    int t;

    public FourD(int x, int y, int z, int t) {
        super(x, y, z);
        this.t = t;
    }
}
