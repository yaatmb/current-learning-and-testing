package chapter14.boundedwildcard;

/**
 * Класс хранит массив координат объектов
 */
public class Coords<T extends TwoD> {
    T[] coords;

    public Coords(T[] coords) {
        this.coords = coords;
    }
}
