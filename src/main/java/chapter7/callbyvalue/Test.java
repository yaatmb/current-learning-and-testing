package chapter7.callbyvalue;

/**
 * Класс, содержащй метод, по сути ничего не изменяющий
 */
public class Test {
    void meth(int i, int j) {
        i *= 2;
        j /= 2;
    }
}