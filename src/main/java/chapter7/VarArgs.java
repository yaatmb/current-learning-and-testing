package chapter7;

/**
 * Продемонстрировать применение аргументов переменной длины
 */
public class VarArgs {
    /**
     * Метод объявляется с аргументами переменной длины
     *
     * @param v
     */
    static void vaTest(int... v) {
        System.out.print("Количество аргументов: " + v.length + " Содержимое: ");
        for (int x : v) {
            System.out.print(x + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        vaTest(10);
        vaTest(1, 2, 3);
        vaTest();
    }
}
