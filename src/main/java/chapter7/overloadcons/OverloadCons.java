package chapter7.overloadcons;

/**
 *
 */
public class OverloadCons {
    public static void main(String[] args) {
        // Создать параллепипеды, используя разные конструкторы
        Box mybox1 = new Box(10, 20, 15);
        Box mybox2 = new Box();
        Box mybox3 = new Box(7);

        double vol;

        // Получить объём первого параллепипеда
        vol = mybox1.volume();
        System.out.println("Объём mybox1 равен " + vol);

        // Получить объём второго параллепипеда
        vol = mybox2.volume();
        System.out.println("Объём mybox2 равен " + vol);

        // Получить объём третьего параллепипеда
        vol = mybox3.volume();
        System.out.println("Объём mybox3 равен " + vol);
    }
}

class Box {
    double width;
    double height;
    double depth;

    /**
     * Конструктор, используемый при указании всех размеров
     *
     * @param w
     * @param h
     * @param d
     */
    Box(double w, double h, double d) {
        width = w;
        height = h;
        depth = d;
    }

    /**
     * Конструктор, используемый, когда ни один из размеров не указан
     */
    Box() {
        width = -1; // использование значения -1 для обозначения
        height = -1; // неинициализированного
        depth = -1; // параллепипеда
    }

    /**
     * Конструктор, используемый при создании куба
     */
    Box(double len) {
        width = height = depth = len;
    }

    /**
     * Рассчитать и возвратить объём
     *
     * @return
     */
    double volume() {
        return width * height * depth;
    }
}