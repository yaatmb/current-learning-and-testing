package chapter7.passobjre;

/**
 * Created by AErmakov on 25.02.2016.
 */
public class Test {
    int a, b;

    Test(int i, int j) {
        a = i;
        b = j;
    }

    /** Передать объект */
    void meth(Test o) {
        o.a *= 2;
        o.b /= 2;
    }
}
