package chapter7.overloaddemo2;

/**
 * Применить автоматическое преобразование типов к перегрузке
 */
public class OverloadDemo {
    void test() {
        System.out.println("Параметры отсутствуют.");
    }

    /**
     * Перегружаемый метод, проверяющий наличие
     * двух целочисленных параметров
     * @param a
     * @param b
     */
    void test(int a, int b) {
        System.out.println("a и b: " + a + " " + b);
    }

    /**
     * Перегружаемый метод, проверяющий наличие
     * параметра типа double
     * @param a
     */
    void test(double a) {
        System.out.println("Внутреннее преобразование при вызове test(double) a: " + a);
    }
}

class Overload {
    public static void main(String[] args) {
        OverloadDemo ob = new OverloadDemo();
        int i = 88;

        ob.test();
        ob.test(10, 20);

        ob.test(i); // здесь вызывается вариант метода test(double)
        ob.test(123.2); // и здесь вызывается вариант метода test(double)
    }
}
