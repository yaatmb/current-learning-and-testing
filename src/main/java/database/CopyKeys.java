package database;

import java.sql.*;

/**
 * ����������� ������
 */
public class CopyKeys {

    public static void main(String[] args) {
        try {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try (Connection sourceConnection = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/p95189", "postgres", "postgres");
             Connection destinationConnection = DriverManager.getConnection("jdbc:oracle:thin:192.168.17.247:1521:orcl", "ERMAKOV_OTPBANK", "123456")) {
            try (PreparedStatement sourceStatement = sourceConnection.prepareStatement("SELECT admin_id, admin_name, admin_position, reg_date, category, " +
                    "status, note, status_date FROM ibank23.admins")) {
                try (ResultSet resultSet = sourceStatement.executeQuery()) {
                    PreparedStatement destinationStatement = destinationConnection.prepareStatement("INSERT INTO ibank23.admins(admin_id, admin_name, " +
                            "admin_position, reg_date, category, status, note, status_date) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");
                    while (resultSet.next()) {
                        int index = 0;
                        destinationStatement.setLong(++index, resultSet.getLong("admin_id"));
                        destinationStatement.setString(++index, resultSet.getString("admin_name"));
                        destinationStatement.setString(++index, resultSet.getString("admin_position"));
                        destinationStatement.setDate(++index, resultSet.getDate("reg_date"));
                        destinationStatement.setInt(++index, resultSet.getInt("category"));
                        destinationStatement.setInt(++index, resultSet.getInt("status"));
                        destinationStatement.setString(++index, resultSet.getString("note"));
                        destinationStatement.setDate(++index, resultSet.getDate("status_date"));
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
