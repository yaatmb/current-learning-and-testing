package database;

import java.io.*;
import java.sql.*;

/**
 * ����������� ������ � ��������� ��� � ����
 */
public class PutToDb {

    public static void main(String[] args) {
        try (Connection connection = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/p70584", "postgres", "postgres")) {
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE ibank23.resources SET content = ? WHERE resource_name='entity/payment' AND app_name='Thoth'");
            preparedStatement.setBytes(1, fromBinaryFile("/home/ermakov/��������/conent.dat"));
            //preparedStatement.setBytes(1, fromBinaryFile("/home/ermakov/��������/resources.csv"));
            if (preparedStatement.executeUpdate() > 0) {
                System.out.println("������ ������� �������");
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    private static byte[] fromBinaryFile(String fileName) throws IOException {
        File file = new File(fileName);
        try (FileInputStream fin = new FileInputStream(file)) {
            byte[] buffer = new byte[fin.available()];
            // ������� ���� � �����
            if (fin.read(buffer, 0, fin.available()) > 0) {
                return buffer;
            }
        }
        return new byte[0];
    }

    private static byte[] fromHexFile(String fileName) throws IOException {
        File file = new File(fileName);
        byte[] buffer;
        try (FileInputStream fin = new FileInputStream(file)) {
            for (int i = 0; i < 5; i++) {
                fin.read();
            }
            buffer = new byte[fin.available()];
            for (int i = 0; i < fin.available() + 2; i++) {
                String str = "" + (char) fin.read();
                buffer[i] = Byte.parseByte(str, 16);
            }
        }
        return buffer;
    }
}
