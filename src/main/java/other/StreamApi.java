package other;

import java.util.HashSet;
import java.util.Set;

/**
 * Демонстрация использования Stream API
 */
public class StreamApi {
    public static void main(String[] args) {
        Set<String> collection = new HashSet<>();
        collection.add("asdad");
        collection.add("asdsfdad");
        collection.add("asdfgad");
        collection.add("asasbdad");
        collection.add("asdfzad");
        collection.add("asdvfvad");
        String s = collection.parallelStream().filter(o -> o.contains("f")).reduce((s1, s2) -> s1 + " " + s2).orElse("");
        System.out.println(s);
    }
}
