package other;

/**
 * Вычленяем из строки "Имя Фамилия" Фамилию
 */
public class SubstringDemo {
    public static void main(String[] args) {
        String user = "Имя Фамилия";
        System.out.println(user.substring(user.lastIndexOf(' ') + 1));
    }
}
