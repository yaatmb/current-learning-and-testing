package other;

/**
 * Демонстрация работы нескольких потоков с одной статической переменной.
 * Необходимо решить задачу так, чтобы всегда в результате counter был равен 20000,
 * при этом каждый поток должен увеличить counter на единицу 10000 раз.
 */
public class MyThread extends Thread {
    private static int counter;

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            counter++;
        }
    }

    public static void main(String[] args) throws InterruptedException {
        MyThread myThread1 = new MyThread();
        MyThread myThread2 = new MyThread();
        myThread1.start();
        myThread2.start();
        myThread1.join();
        myThread2.join();
        System.out.println(counter);
    }
}
