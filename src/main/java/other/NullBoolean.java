package other;

/**
 * Проверим, что будет, если сравнивать Boolean, равный null
 * Должен вернуться NullPointerException
 */
public class NullBoolean {
    public static void main(String[] args) {
        Boolean bool = null;
        if (bool) {
            System.out.println("Good!");
        }
    }
}
