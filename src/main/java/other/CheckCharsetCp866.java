package other;

import sun.nio.cs.IBM866;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

/**
 * Замена неподходящих к кодировке CP866 символов на подчёркивания
 */
public class CheckCharsetCp866 {
    public static void main(String[] args) {
        String s = "72807160_Иванов_Ivan_РРџ_РќРћР’_2.xlsx";
        Charset charset = new IBM866();
        CharsetEncoder ce = charset.newEncoder();
        StringBuilder sb = new StringBuilder(s.length());
        for (char c : s.toCharArray()) {
            sb.append(ce.canEncode(c) ? c : '_');
        }
        System.out.println(sb.toString());
    }
}
