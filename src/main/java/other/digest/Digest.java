package other.digest;

import java.util.HashMap;
import java.util.Map;

/**
 * Задание №3 из Яндекса
 */
public abstract class Digest {
    private final Map<byte[], byte[]> cache = new HashMap<>();

    public byte[] digest(byte[] input) {
        byte[] result = cache.get(input);
        if (result == null) {
            byte[] digest = doDigest(input);
            synchronized (cache) {
                result = cache.get(input);
                if (result == null) {
                    result = digest;
                    cache.put(input, result);
                }
            }
        }
        return result;
    }

    protected abstract byte[] doDigest(byte[] input);
}