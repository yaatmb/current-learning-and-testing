package other.digest;

import java.util.ArrayList;
import java.util.List;

/**
 * Запуск нескольких клиентов в параллельных потоках.
 */
public class Main {
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        List<SimpleDigestClient> simpleDigestClientList = new ArrayList<>();
        DigestCache cache = new DigestCache();
        for (int i = 0; i < 10000; i++) {
            simpleDigestClientList.add(new SimpleDigestClient(cache));
        }
        for (SimpleDigestClient simpleDigestClient : simpleDigestClientList) {
            simpleDigestClient.start();
        }
        for (SimpleDigestClient simpleDigestClient : simpleDigestClientList) {
            try {
                simpleDigestClient.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        long endTime = System.currentTimeMillis();
        System.out.println("Result = " + (endTime - startTime) + " mills.");
    }
}
