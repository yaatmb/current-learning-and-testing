package other.digest;

/**
 * Реализация абстрактного класса Digest
 */
public class DigestCache extends Digest {
    @Override
    protected byte[] doDigest(byte[] input) {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        byte[] result = input.clone();
        result[0] = 1;
        return result;
    }
}
