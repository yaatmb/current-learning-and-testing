package other.digest;

import java.util.Arrays;

/**
 * Простой клиент, который будет получать информацию из DigestCache
 */
public class SimpleDigestClient extends Thread {

    Digest cache;

    public SimpleDigestClient(Digest cache) {
        this.cache = cache;
    }

    @Override
    public void run() {
        super.run();
        byte[] a, b, c, d, e, f, g;
        a = new byte[]{0, 1};
        b = new byte[]{0, 2};
        c = new byte[]{0, 3};
        d = new byte[]{0, 4};
        e = new byte[]{0, 1};
        f = new byte[]{0, 2};
        g = new byte[]{0, 3};
        print("a", a);
        print("b", b);
        print("c", c);
        print("d", d);
        print("e", e);
        print("f", f);
        print("g", g);
        print("a", a);
        print("b", b);
        print("c", c);
        print("d", d);
        print("e", e);
        print("f", f);
        print("g", g);
    }

    private void print(String varName, byte[] var) {
//        System.out.println(Thread.currentThread().getName() + " : " + varName + " : " + Arrays.toString(this.cache.digest(var)));
    }
}
