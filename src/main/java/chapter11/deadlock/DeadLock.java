package chapter11.deadlock;

/**
 * Деонстрация взаимной блокировки
 */
public class DeadLock implements Runnable {
    Locker locker1 = new Locker();
    Locker locker2 = new Locker();

    DeadLock() {
        Thread.currentThread().setName("Главный поток");
        Thread otherThread = new Thread(this, "Соперничащий поток");
        otherThread.start();
        locker1.foo(locker2);
        System.out.println("Назад в главный поток.");
    }

    @Override
    public void run() {
        locker2.foo(locker1);
        System.out.println("Назад в другой поток.");
    }

    public static void main(String[] args) {
        new DeadLock();
    }
}
