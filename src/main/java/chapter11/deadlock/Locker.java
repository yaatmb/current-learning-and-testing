package chapter11.deadlock;

/**
 * Участник взаимной блокировки
 */
public class Locker {
    synchronized public void foo(Locker otherLocker) {
        String name = Thread.currentThread().getName();

        System.out.println(name + " вошёл в метод foo.");

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            System.out.println("Поток " + name + " перван.");
        }

        System.out.println(name + " пытается вызвать метод otherLocker.last().");

        otherLocker.last();
    }

    synchronized private void last() {
        String name = Thread.currentThread().getName();
        System.out.println("В методе " + name + ".last()");
    }
}
