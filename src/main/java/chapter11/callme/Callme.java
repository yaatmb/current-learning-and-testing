package chapter11.callme;

/**
 * Содержит метод, который выводит открывающуюся скобку с сообщением,
 * ждёт секунду и выводит закрывающуюся скобку
 */
public class Callme {
    synchronized public void call(String msg) {
        System.out.print("[" + msg);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("Поток прерван");
        }
        System.out.println("]");
    }
}
