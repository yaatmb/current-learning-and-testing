package chapter11.callme;

/**
 * Вывод нескольких строк в отдельных потоках
 */
public class Synch {
    public static void main(String[] args) {
        Callme target = new Callme();
        Caller caller1 = new Caller(target, "Добро пожаловать");
        Caller caller2 = new Caller(target, "в синхронизированный");
        Caller caller3 = new Caller(target, "мир!");

        // ожидать завершение потока исполнения
        try {
            caller1.thread.join();
            caller2.thread.join();
            caller3.thread.join();
        } catch (InterruptedException e) {
            System.out.println("Главный поток прерван");
        }
    }
}
