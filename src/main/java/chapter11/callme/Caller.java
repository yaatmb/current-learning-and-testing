package chapter11.callme;

/**
 * Создаёт отдельный поток и вызывает метод call у объекта класса Callme
 */
public class Caller implements Runnable {
    String message;
    Callme target;
    Thread thread;

    public Caller(Callme target, String message) {
        this.target = target;
        this.message = message;
        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        target.call(message);
    }
}
