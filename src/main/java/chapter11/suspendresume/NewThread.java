package chapter11.suspendresume;

/**
 * Поток, который может приостанавливаться и возобновляться
 */
public class NewThread implements Runnable {
    /** Имя потока исполнения */
    String name;

    /** Текущий поток исполнения */
    Thread thread;

    /** Флаг приостановки потока */
    public boolean suspendFlag;

    NewThread(String name) {
        this.name = name;
        thread = new Thread(this, name);
        System.out.println("Новый поток: " + name);
        suspendFlag = false;
        thread.start();
    }

    @Override
    public void run() {
        try {
            for (int i = 15; i > 0; i--) {
                System.out.println(name + ": " + i);
                Thread.sleep(200);
                synchronized (this) {
                    while (suspendFlag) {
                        wait();
                    }
                }
            }
        } catch (InterruptedException e) {
            System.out.println(name + " прерван.");
        }
        System.out.println(name + " завершён.");
    }

    synchronized public void mysuspend() {
        suspendFlag = true;
    }

    synchronized public void myresume() {
        suspendFlag = false;
        notify();
    }
}
