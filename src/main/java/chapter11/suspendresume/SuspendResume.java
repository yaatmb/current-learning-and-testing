package chapter11.suspendresume;

/**
 * Приостановка и возобновление иполнения потока современным способом
 */
public class SuspendResume {
    public static void main(String[] args) {
        NewThread thread1 = new NewThread("Один");
        NewThread thread2 = new NewThread("Два");

        try {
            Thread.sleep(600);
            thread1.mysuspend();
            System.out.println("Приостановка потока Один");
            Thread.sleep(600);
            thread1.myresume();
            System.out.println("Возобновление потока Один");
            Thread.sleep(600);
            thread2.mysuspend();
            System.out.println("Приостановка потока Два");
            Thread.sleep(600);
            thread2.myresume();
            System.out.println("Возобновление потока Два");
            Thread.sleep(600);
        } catch (InterruptedException e) {
            System.out.println("Главный поток прерван.");
        }

        try {
            System.out.println("Ожидание завершения потоков.");
            thread1.thread.join();
            thread2.thread.join();
        } catch (InterruptedException e) {
            System.out.println("Главный поток прерван.");
        }
        System.out.println("Главный поток завершён.");
    }
}
