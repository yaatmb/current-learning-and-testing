package chapter11.joindemo;

/**
 * емонстрация применения метода join()
 */
public class JoinDemo {
    public static void main(String[] args) {
        NewThread thread1 = new NewThread("Один");
        NewThread thread2 = new NewThread("Два");
        NewThread thread3 = new NewThread("Три");
        System.out.println("Поток один запущен: " + thread1.thread.isAlive());
        System.out.println("Поток два запущен: " + thread2.thread.isAlive());
        System.out.println("Поток три запущен: " + thread3.thread.isAlive());
        try {
            thread1.thread.join();
            thread2.thread.join();
            thread3.thread.join();
        } catch (InterruptedException e) {
            System.out.println("лавный поток прерван");
        }
        System.out.println("Поток один запущен: " + thread1.thread.isAlive());
        System.out.println("Поток два запущен: " + thread2.thread.isAlive());
        System.out.println("Поток три запущен: " + thread3.thread.isAlive());
    }
}
