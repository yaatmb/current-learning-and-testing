import java.util.Calendar;

/**
 * ������������ ���������� ������ ������� GregorianCalendar#add ��� ��������� DAY_OF_MONTH ��� DAY_OF_YEAR � �������� �������� filed
 */
public class CalendarAdd {

    public static void main(String[] args) {
        Calendar cal = getCalendar();
        cal.add(Calendar.DAY_OF_MONTH, 1);
        System.out.println(cal.getTime());
        cal = getCalendar();
        cal.add(Calendar.DAY_OF_YEAR, 1);
        System.out.println(cal.getTime());
    }

    private static Calendar getCalendar() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2012);
        cal.set(Calendar.MONTH, 1);
        cal.set(Calendar.DAY_OF_MONTH, 29);
        return cal;
    }
}
