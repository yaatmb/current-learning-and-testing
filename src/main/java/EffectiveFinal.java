/**
 * Created by unlim on 28.06.17.
 */

import java.util.ArrayList;
import java.util.List;

public class EffectiveFinal {
    public static void main(String[] args) {
       /* ������ � ������������� 'final', �� �����������
         *  �������� ������ ����� �������������
         */
        final String firstName = "Alex";

        /* ���������� 'final' ���������� � ���������� Runnable
          * ��� � ����� ��������� ������
          */
        Thread thread = new Thread(new Runnable() {
            public void run() {
                System.out.println("Thread: " + firstName);
            }
        });

        /*��������� ����� ��� ����������� �������� ���������� firstName*/
        thread.start();

        List listStrings = new ArrayList<>();
        listStrings.add("Alex");
        listStrings.add("Nick");
        /*
        * lastName - effectively final ����������, ������������������ ��������
        * � ������� ����� ���� ������������ � lambda-���������
        * */
        String lastName = "Smith";
        /*
        * ���� �� �������� �������� ���������� lastName, �� �� �� �������
        * ������������ ������ ���������� � lambda-���������
        * */
//        lastName += "";
        listStrings.stream()
                .forEach(str -> System.out.println("Lambda expression: " + str
                + " " + lastName));

    }
}
