/**
 * �� ����� ��������, ��� ��� ������������ ����� ������ ����������, ����������� �� ������ ������, �������� �� ������, �� ������� ��������� ����������,
 * � ������, ������������ � ����������.<br/>
 * IDE ������������, ��� ���������� ���������� ����� �� ������������, �.�. cwgs.getS() � ���������� s ��������� �� 2 ������ �������.
 */
public class ChangeStringByGetMethod {

    public static void main(String[] args) {

        ClassWithGetString cwgs = new ClassWithGetString();
        cwgs.setS("������!");
        System.out.println(cwgs.getS());
        String s = cwgs.getS();
        s = "����!";
        System.out.println(cwgs.getS());
    }

    private static class ClassWithGetString {

        String s;

        String getS() {
            return s;
        }

        void setS(String s) {
            this.s = s;
        }
    }
}