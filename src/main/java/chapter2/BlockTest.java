package chapter2;

/**
 * Продемонстрировать применение блока кода.
 * 2.5.chapter2.BlockTest
 */
public class BlockTest {
    public static void main(String[] args) {
        int x, y;

        y = 20;

        // адресатом этого операнда цикла служит блок кода
        for (x = 0; x < 10; x++) {
            System.out.println("Значение x: " + x);
            System.out.println("Значение y: " + y);
            y = y - 2;
        }
    }
}
