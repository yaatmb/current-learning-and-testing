package chapter2;

/**
 * Это простая программа на Java
 * 2.1.chapter2.SimpleProgramOnJava
 */
public class SimpleProgramOnJava {
    // Эта программа начинается  вызова метода main()
    public static void main(String[] args) {
        System.out.println("Простая программа на Java.");
    }
}
