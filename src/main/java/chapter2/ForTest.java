package chapter2;

/**
 * Продемонстрировать применение  цикла for.
 * 2.4.chapter2.ForTest
 */
public class ForTest {
    public static void main(String[] args) {
        int x;

        for (x = 0; x < 10; x++)
            System.out.println("Значение x: " + x);
    }
}
