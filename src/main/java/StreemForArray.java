import java.util.Arrays;

/**
 * [2, 6, 8, 3, 5, 6, 3, 5, 1, 6, 8] => [2, 6, 8, 3, 5, 1]
 */
public class StreemForArray {

    public static void main(String[] args) {
        int[] m = {2, 6, 8, 3, 5, 6, 3, 5, 1, 6, 8};
        StringBuilder sb = new StringBuilder();
        Arrays.stream(m).distinct().forEach(value -> {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(value);
        });
        System.out.println(sb);
    }
}
