package chapter18.phonebook;

import java.io.*;
import java.util.Properties;

/**
 * Реализация простенького телефонного справочника в файле (минимум функций)
 */
public class PhoneBook {

    private Properties phones;
    private BufferedReader reader;
    private String filename;
    private boolean changed;

    public static void main(String[] args) {
        new PhoneBook().go();
    }

    public void go() {
        phones = new Properties();
        reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Телефонная книга версии 0.1");
        System.out.println("Автор: Артём Ермаков");
        int c = 0;
        do {
            System.out.println();
            System.out.println("Выберите действие:");
            System.out.println("О - открыть");
            System.out.println("Д - добавить запись");
            System.out.println("С - сохранить телефонную книгу");
            System.out.println("П - печать всей телефонной книги");
            System.out.println("В - выход");
            try {
                c = reader.readLine().charAt(0);
            } catch (IOException e) {
                e.printStackTrace();
            }
            switch (c) {
                case 'О':
                    openFile();
                    break;
                case 'Д':
                    addPhone();
                    break;
                case 'П':
                    printPhoneBook();
                    break;
                case 'С':
                    storePhoneBook();
                    break;
                case 'В':
                    break;
                default:
                    System.out.println("Такой команды не существует, ввод команды осуществляется на русском языке.");
            }
        } while (c != 'В');
    }

    private void storePhoneBook() {
        if (changed) {
            if (filename == null) {
                System.out.println("Введите имя файла (без расширения .dat)");
                try {
                    filename = reader.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                filename += ".dat";
            }
            try(FileOutputStream file = new FileOutputStream(filename)) {
                phones.store(file, "Телефонная книга версии 0.1");
                System.out.println("Телефонна книга успешно сохранена в файле: " + filename);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("В телефонной книге не было никаких изменений, сохранение не требуется.");
        }
    }

    private void addPhone() {
        System.out.println("Введите имя:");
        String name = null;
        try {
            name = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Введите номер телефона");
        String phone = null;
        try {
            phone = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        phones.setProperty(name, phone);
        changed = true;
        System.out.println("В телефонный справочник добавлена запись:");
        System.out.println(name + ": " + phone);
    }

    private void printPhoneBook() {
        if (phones.size() == 0) {
            System.out.println("Телефонный справочник пуст.");
        } else {
            System.out.println("Содержимое телефонного справочника:");
            phones.forEach((name, phone) -> System.out.println(name + ": " + phone));
        }
    }

    private void openFile() {
        System.out.println("Введите имя файла телефонной книги (без расширения .dat):");
        filename = null;
        try {
            filename = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (filename != null) {
            filename += ".dat";
            try (FileInputStream file = new FileInputStream(filename)) {
                try {
                    phones.load(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                System.out.println("Такого файла не существует, загружена пустая телефонная книга.");
            }
        }
        changed = false;
        System.out.println("Файл " + filename + " успешно загружен.");
    }
}
