package chapter18;

import java.util.TreeSet;

/**
 * Применение отсортированного по кампаратору множества с использованием лямбда-выражения
 */
public class ComparatorDemo {
    public static void main(String[] args) {
        TreeSet<String> fruits = new TreeSet<>((a, b) -> b.compareTo(a));
        fruits.add("Арбуз");
        fruits.add("Дыня");
        fruits.add("Яблоко");
        fruits.add("Ананас");
        fruits.add("Манго");
        fruits.add("Грейпфрут");
        fruits.add("Груша");
        fruits.add("Апельсин");
        fruits.add("Мандарин");
        fruits.add("Гранат");

        for (String fruit : fruits) {
            System.out.println(fruit);
        }
    }
}
