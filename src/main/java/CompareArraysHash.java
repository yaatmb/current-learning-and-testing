import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by unlim on 30.06.17.
 */
public class CompareArraysHash {
    public static void main(String[] args) {
        Map<byte[], byte[]> map = new HashMap<>();
        byte[] firstKey = {1, 2 ,3, 4, 5};
        byte[] secondKey = {1, 2 ,3, 4, 5};
        System.out.println("firstKey.equals(secondKey): " + (firstKey.equals(secondKey)));
        System.out.println("Arrays.equals(firstKey, secondKey): " + (Arrays.equals(firstKey, secondKey)));
        map.put(firstKey, doDigest(firstKey));
        byte[] valuesBySecondKey = map.get(secondKey); // ��� ������ null, �.�. ���� �� byte[] - ��������� �������
        for (int i = 0; i < valuesBySecondKey.length; i++) {
            System.out.println("valuesBySecondKey[" + i + "] = " + valuesBySecondKey[i]);
        }
    }

    private static byte[] doDigest(byte[] input) {
        byte[] result = new byte[input.length];
        for (int i = 0; i < input.length; i++) {
            result[i] = (byte) (input[i] + 1);
        }
        return result;
    }
}
