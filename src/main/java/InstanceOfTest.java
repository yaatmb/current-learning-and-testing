/**
 * Created by unlim on 13.07.17.
 */
public class InstanceOfTest {
    public static void main(String[] args) {
        A a = new B();
        A b = new B();
        C c = new C();
        D d = new D();
        System.out.println(a instanceof C);
        System.out.println(b instanceof B);
        System.out.println(c instanceof B);
        //System.out.println(d instanceof A);
    }
}

class A {

}


class B extends A {

}

class C extends B {

}

class D {

}
