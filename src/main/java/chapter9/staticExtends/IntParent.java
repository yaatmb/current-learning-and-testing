package chapter9.staticExtends;

/**
 * Родительский интерфейс со статическим методом
 */
public interface IntParent {
    static void test() {
        System.out.println("test from IntParent");
    }
}
