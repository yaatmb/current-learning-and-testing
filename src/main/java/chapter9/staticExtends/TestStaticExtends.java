package chapter9.staticExtends;

/**
 * Проверка наследования статических методов
 */
public class TestStaticExtends {
    public static void main(String[] args) {
        IntParent intParent;
        IntChild intChild;

        Parent parent = new Parent();
        Child child = new Child();

        IntParent.test();
        // IntChild.test(); // не вариант
        // intParent.test(); // не вариант
        // intChild.test(); // не вариант

        Parent.test();
        Child.test();
        parent.test();
        child.test();
    }
}
