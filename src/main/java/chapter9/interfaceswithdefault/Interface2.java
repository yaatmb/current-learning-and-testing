package chapter9.interfaceswithdefault;

/**
 * Второй интерфейс с дефолтным методом
 */
public interface Interface2 {
    default void firstMethod() {
        System.out.println("firstMethod from Interface2");
    }
}
