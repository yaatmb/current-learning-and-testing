package chapter9.interfaceswithdefault;

/**
 * Первый интерфейс с дефолтным методом
 */
public interface Interface1 {
    default void firstMethod() {
        System.out.println("firstMethod from Interface1");
    }
}
