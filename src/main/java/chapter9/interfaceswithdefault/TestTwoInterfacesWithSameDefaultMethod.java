package chapter9.interfaceswithdefault;

/**
 * Класс, реализующий конфликтующие интерфейсы
 */
public class TestTwoInterfacesWithSameDefaultMethod implements Interface1, Interface2{
    public static void main(String[] args) {
        TestTwoInterfacesWithSameDefaultMethod test = new TestTwoInterfacesWithSameDefaultMethod();
        test.firstMethod();
    }

    /**
     * Пришлось переопределить метод, т.к. неизвестно из какого интерфейса нужно взять реализацию по умолчанию
     */
    @Override
    public void firstMethod() {
        System.out.println("firstMethod from TestTwoOnterfacesWithSameDefaultMethod");
    }
}
