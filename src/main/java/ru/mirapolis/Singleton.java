package ru.mirapolis;

/**
 * Потоко-безопасная реализация паттерна Singleton
 */
public class Singleton {
    private static Singleton singleton;

    private Singleton() {
    }

    public static Singleton getInstance() {
        if (singleton == null) {
            createInstance();
        }
        return singleton;
    }

    private static synchronized void createInstance() {
        if (singleton == null) {
            singleton = new Singleton();
        }
    }
}
