package ru.mirapolis;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Класс для хранения информации о проекте и входящих в него пакетах.
 */
public class Project {
    private String name; //название проекта
    private List<Package> packages; //список пакетов проекта

    public Project(String name) {
        this.name = name;
    }

    /**
     * Проверяет наличие циклических зависимостей между пакетами проекта.
     * Например, A->B->A, A->B->C->A
     */
    public Boolean hasCyclicDependencies() {
        for (Package aPackage : packages) {
            if (aPackage.hasCyclicDependencies()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Упорядоченный список пакетов проекта для компиляции с учетом их
     * зависимостей.
     */
    public List<Package> getCompilationOrder() {
        if (hasCyclicDependencies()) {
            throw new RuntimeException("Есть наличие циклических зависимостей между пакетами проекта.");
        }
        List<Package> packagesToAdd = new ArrayList<>(packages);
        LinkedList<Package> addedPackages = new LinkedList<>();
        while (!packagesToAdd.isEmpty()) {
            Iterator<Package> iterator = packagesToAdd.iterator();
            while (iterator.hasNext()) {
                Package aPackage = iterator.next();
                if (aPackage.getDependencies().isEmpty() || addedPackages.containsAll(aPackage.getDependencies())) {
                    addedPackages.add(aPackage);
                    iterator.remove();
                }
            }
        }
        return addedPackages;
    }

}
