package ru.mirapolis;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Класс для хранения информации о пакетах и их зависимостях.
 */
public class Package {
    private String name; //уникальное название пакета
    private List<Package> dependencies; //список пакетов, от которых зависит данный

    public Package(String name)  {
        this.name = name;
    }

    public List<Package> getDependencies() {
        return dependencies;
    }

    public String getName(){
        return name;
    }

    public boolean hasCyclicDependencies() {
        return hasCyclicDependencies(new ArrayList<>());
    }

    private boolean hasCyclicDependencies(List<Package> parents) {
        if (dependencies.isEmpty()) {
            return false;
        }
        // ищем пересечение зависимых и зависящих пакетов
        if (!parents.stream().filter(aPackage -> dependencies.contains(aPackage)).collect(Collectors.toList()).isEmpty()) {
            // если пересечение есть
            return true;
        }
        // рекурсивно проваливаемся всё глубже в зависимости, пока не найдём циклическую зависимость
        for (Package dependency : dependencies) {
            List<Package> newParents = new ArrayList<>(parents);
            newParents.add(dependency);
            if (!dependency.hasCyclicDependencies(newParents)) {
                return true;
            }
        }
        return false;
    }

}
