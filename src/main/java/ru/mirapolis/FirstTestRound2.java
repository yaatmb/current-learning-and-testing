package ru.mirapolis;

import java.util.Arrays;

/**
 * Количество слов в строке
 */
public class FirstTestRound2 {
    public static void main(String[] args) {
        String s = " gsdfg fsgd fsdghsjkfhskl    ghgh skldjfh   sdklfgh sldfj sdlfsh dlkfj    ";
        System.out.println(countWords(s));
        int[] mas = {2, 1, 4, 2, 3};
        int[] newMas = removeDuplicates(mas);
        for (int element : newMas) {
            System.out.print(element + " ");
        }
        System.out.println();
    }

    /**
     * @param s – Строка состоящая из букв латинского алфавита и пробелов
     * @return Количество слов в строке s
     */
    public static int countWords(String s) {
        return s.trim().split("\\s+").length;
    }

    /**
     * @param a - оригинальный массив
     * @return массив, который не содержит дубликатов. Из последовательности
     * дубликатов оставлять первый элемент, например, {2,1,4,2,3} -> {2,1,4,3}
     */
    public static int[] removeDuplicates(int[] a) {
        return Arrays.stream(a).distinct().toArray();
    }
}
